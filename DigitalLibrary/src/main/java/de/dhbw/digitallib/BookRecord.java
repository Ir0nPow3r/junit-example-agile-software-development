package de.dhbw.digitallib;

import java.time.LocalDate;
import java.util.Objects;

/**
 * {@link Book} class as record.
 */
public final class BookRecord {
    private final String author;
    private final String title;
    private final LocalDate publishDate;

    BookRecord(String author, String title, LocalDate publishDate) {
        this.author = author;
        this.title = title;
        this.publishDate = publishDate;
    }

    public String author() {
        return author;
    }

    public String title() {
        return title;
    }

    public LocalDate publishDate() {
        return publishDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (BookRecord) obj;
        return Objects.equals(this.author, that.author) &&
                Objects.equals(this.title, that.title) &&
                Objects.equals(this.publishDate, that.publishDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(author, title, publishDate);
    }

    @Override
    public String toString() {
        return "BookRecord[" +
                "author=" + author + ", " +
                "title=" + title + ", " +
                "publishDate=" + publishDate + ']';
    }
}
